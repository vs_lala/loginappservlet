<%@page import="com.bitwise.loginapp.UserProfile"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.sun.org.apache.xalan.internal.xsltc.runtime.Attributes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="layout/_header.jsp" %>

	<%
		UserProfile user = (UserProfile) this.getServletContext().getAttribute("user");
	%>
	
	<ul class="list">
		<li class="list-item"><%= user.getFirstName() %></li>
		<li class="list-item"><%= user.getLastName() %></li>
		<li class="list-item"><%= user.getEmail() %></li>
		<li class="list-item"><%= user.getMobile() %></li>
	</ul>
	
	<br />
	
	<a href="LogoutServlet">Logout</a>

<%@ include file="layout/_footer.jsp" %>