package com.bitwise.loginapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class ValidateFilter
 */
//@WebFilter("/ValidateFilter")
public class ValidateFilter implements Filter {

    /**
     * Default constructor. 
     */
    public ValidateFilter() {
        
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		res.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		if (req.getSession(false).getAttribute("username") == null) {
			out.println("<h1>Session Expired</h1>");
			res.sendRedirect("login.jsp");
			return;
		}
		
		String page = "";
		
		if (req.getMethod().equalsIgnoreCase("POST")) {
			String firstName = req.getParameter("first_name");
			String lastName = req.getParameter("last_name");
			String email = req.getParameter("email");
			String mobile = req.getParameter("mobile");
			String password = req.getParameter("password");
			
			
			page = "ProfileServlet";
			boolean flag = false;
			
			if ((Validator.validateString(firstName) 
					&& Validator.validateString(lastName))) {
				System.out.println("string validation");
				out.println("<span class='invalid'>Invalid Name String!</span> ");
	
				flag = true;
			}
			
			if (! Validator.validateEmail(email)) {
				System.out.println("email validation");
				out.println("<span class='invalid'>Invalid Email Address</span> ");
		
				flag = true;
			}
			
			if (! Validator.validatePassword(password)) {
				System.out.println("password validation");
				out.println("<span class='invalid'>Password must contain 1 uppercase,"
						+ " 1 lowercase, minimum of 6 chars and max of 20 chars and a"
						+ " symbol</span> ");
			
				flag = true;
			}
			
			if (flag) {
				req.getRequestDispatcher("login.jsp").include(req, res);
				return;
			}
				
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
