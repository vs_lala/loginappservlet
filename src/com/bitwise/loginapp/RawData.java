package com.bitwise.loginapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RawData {
	
	private static final Map<String, String> userCredentials = new HashMap<>();
	
	public RawData() {
		this.init();
	}

	private void init() {
		ArrayList<String> usernames = new ArrayList<String>();
		ArrayList<String> passwords = new ArrayList<String>();
		
		usernames.add("pikachu");
		usernames.add("balbasaur");
		usernames.add("charizard");
		usernames.add("richu");
		usernames.add("meow");
		usernames.add("pegion");
		usernames.add("catapillar");
		
		passwords.add("electric");
		passwords.add("plants");
		passwords.add("fire");
		passwords.add("electric");
		passwords.add("cat");
		passwords.add("wind");
		passwords.add("shield");
		
		for (int i=0; i < usernames.size(); i++) {
			RawData.userCredentials.put(usernames.get(i), passwords.get(i));
		}		
	}
	
	public static Map getUserCredential () {
		return RawData.userCredentials;
	}
}
