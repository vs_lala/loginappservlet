<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div>
	<h1>Invalid Parameters!</h1>
	<ul>
		<li>Name Must be a string without numeric value</li>
		<li>Email must be a valid email address</li>
		<li>Please Enter a Valid Mobile Number</li>
		<li>Password must contain 1 Lowercase, 1 uppercase, 1 special symbole,
		minimum length = 6, max length = 20</li>
	</ul> 
	<br/>
	<a href="profile.jsp">Try Again!</a>
	</div>
</body>
</html>