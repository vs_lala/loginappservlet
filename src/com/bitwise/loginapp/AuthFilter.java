package com.bitwise.loginapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

/**
 * Servlet Filter implementation class AuthFilter
 */
//@WebFilter("/AuthFilter")
public class AuthFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthFilter() {
       
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		System.out.println(new RawData().getUserCredential().toString());
		if (req.getMethod().equalsIgnoreCase("POST")) {
			System.out.println("post request h");
			if (new RawData().getUserCredential().containsKey(username)) {
				if (new RawData().getUserCredential().get(username).equals(password)) {
					req.getSession(true).setAttribute("username", username);
					res.sendRedirect("profile.jsp");
				}
			} else {
				new PrintWriter(response.getWriter()).println("<h1>Invalid Credentials</h1>");
				chain.doFilter(req, res);
			}
		} else {
			chain.doFilter(request, response);
		}

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
